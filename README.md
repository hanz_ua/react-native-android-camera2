
# react-native-android-camera2

## Getting started

`$ npm install react-native-android-camera2 --save`

### Mostly automatic installation

`$ react-native link react-native-android-camera2`

### Manual installation


#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.kombine.RNAndroidcamera2Package;` to the imports at the top of the file
  - Add `new RNAndroidcamera2Package()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-android-camera2'
  	project(':react-native-android-camera2').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-android-camera2/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-android-camera2')
  	```


## Usage
```javascript
import RNAndroidcamera2 from 'react-native-android-camera2';

// TODO: What to do with the module?
RNAndroidcamera2;
```
