package com.kombine;

import android.Manifest;
import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Range;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Semaphore;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class PhotoCameraView extends RNAndroidCamera2Base {
  private static final String TAG = "AndroidCameraApi";
  private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

  static {
    ORIENTATIONS.append(Surface.ROTATION_0, 90);
    ORIENTATIONS.append(Surface.ROTATION_90, 0);
    ORIENTATIONS.append(Surface.ROTATION_180, 270);
    ORIENTATIONS.append(Surface.ROTATION_270, 180);
  }


  public PhotoCameraView(Context context, Activity activity) {
    super(context);
    this.activity = activity;
  }

  private Activity activity;
  private String cameraId;
  protected CameraDevice cameraDevice;
  protected CameraCaptureSession cameraCaptureSessions;
  protected CaptureRequest.Builder captureRequestBuilder;
  private Size imageDimension;
  private ImageReader imageReader;
  private static final int REQUEST_CAMERA_PERMISSION = 200;
  private Handler mBackgroundHandler;
  private HandlerThread mBackgroundThread;
  private View flashing;

  public void setFlashing(View flashing) {
    this.flashing = flashing;
  }

  private TextureView.SurfaceTextureListener mSurfaceTextureListener
    = new TextureView.SurfaceTextureListener() {

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
      openCameraVideo(width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
      configureTransform(width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
      return true;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }
  };


  TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
      openCamera(width, height);
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
      configureTransform(width, height);
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
      return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }
  };


  private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
    @Override
    public void onOpened(@NonNull CameraDevice camera) {
      cameraDevice = camera;
      createCameraPreview();
    }

    @Override
    public void onDisconnected(@NonNull CameraDevice camera) {
      cameraDevice.close();
    }

    @Override
    public void onError(@NonNull CameraDevice camera, int error) {
      cameraDevice.close();
      cameraDevice = null;
    }
  };

  public void initPhoto() {
    isEnable = true;
    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_DENIED) {
      startBackgroundThread();
      setSurfaceTextureListener(textureListener);
    }
  }

  public void initVideo() {
    isEnableVideo = true;
    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_DENIED) {
      //onResume();
      setSurfaceTextureListener(mSurfaceTextureListener);
      //openCameraVideo(1920, 1080);
    }

  }

  public PhotoCameraView(Context context) {
    super(context);
  }

  protected void startBackgroundThread() {
    mBackgroundThread = new HandlerThread("Camera Background");
    mBackgroundThread.start();
    mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
  }

  private void stopBackgroundThread() {
    if (mBackgroundThread == null) {
      return;
    }
    mBackgroundThread.quitSafely();
    try {
      mBackgroundThread.join();
      mBackgroundThread = null;
      mBackgroundHandler = null;
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }


  private void initZoom() {
    if (zoom != null)
      captureRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom);
    //captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, brightness);
  }

  private void initSetting() {
    initZoom();
    if (cameraCaptureSessions != null) {
      try {
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, false);
        cameraCaptureSessions.capture(captureRequestBuilder.build(), PhotoCameraView.this.captureCallbackHandler, mBackgroundHandler);
        //Now add a new AF trigger with focus region

        if (focusAreaTouch != null)
          if (isMeteringAreaAFSupported()) {
            captureRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, new MeteringRectangle[]{focusAreaTouch});
          }
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        //brightness
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, brightness);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        captureRequestBuilder.setTag("FOCUS_TAG"); //we'll capture this later for resuming the preview
        new Handler().postDelayed(new Runnable() {
          @Override
          public void run() {
            //captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
            try {
              captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, true);
            } catch (Exception e) {
              e.printStackTrace();
            }
          }
        }, 500);
        //then we ask for a single request (not repeating!)
        cameraCaptureSessions.capture(captureRequestBuilder.build(), PhotoCameraView.this.captureCallbackHandler, mBackgroundHandler);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private ImageReader reader;

  private void createFolder() {
    File folder = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath() + "/.media");
    if (!folder.exists()) {
      folder.mkdirs();
    }
  }

  protected void takePicture() {
    if (null == cameraDevice) {
      Log.e(TAG, "cameraDevice is null");
      return;
    }
    CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
    try {
      CameraCharacteristics characteristics = Objects.requireNonNull(manager).getCameraCharacteristics(cameraId);

      StreamConfigurationMap map = characteristics.get(
        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
      if (map == null) {
        return;
      }
      Size largest = Collections.max(
        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
        new RNAndroidCamera2View.CompareSizesByArea());
      reader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 1);
      final List<Surface> outputSurfaces = new ArrayList<>(2);

      SurfaceTexture texture = getSurfaceTexture();
      texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

//      captureRequestBuilder =
//        cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
    // surface = new Surface(texture);
      outputSurfaces.add(reader.getSurface());
      outputSurfaces.add(surface);
      //outputSurfaces.add(new Surface(texture));
      captureRequestBuilder.addTarget(surface);
      captureRequestBuilder.addTarget(reader.getSurface());
      // initSetting();
      // Orientation
      int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
      captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));
      createFolder();
      final File file = new File(activity.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath() + "/.media" + "/IMG_" + System.currentTimeMillis() + ".jpg");
      ImageReader.OnImageAvailableListener readerListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
          Image image = null;
          try {
            image = reader.acquireLatestImage();
            ByteBuffer buffer = image.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.capacity()];
            buffer.get(bytes);
            save(bytes);

          } catch (FileNotFoundException e) {
            e.printStackTrace();
          } catch (IOException e) {
            e.printStackTrace();
          } finally {
            if (image != null) {
              image.close();
            }
          }
        }

        private void save(byte[] bytes) throws IOException {
          OutputStream output = null;
          try {
            output = new FileOutputStream(file);
            output.write(bytes);
          } finally {
            if (null != output) {
              output.close();
            }
          }
        }
      };

      reader.setOnImageAvailableListener(readerListener, mBackgroundHandler);

      cameraDevice.createCaptureSession(outputSurfaces, new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
          try {
            session.stopRepeating();
            session.abortCaptures();
            session.capture(captureRequestBuilder.build(), new CameraCaptureSession.CaptureCallback() {
              @Override
              public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                             @NonNull CaptureRequest request,
                                             @NonNull TotalCaptureResult result) {
                super.onCaptureCompleted(session, request, result);
                cameraCaptureSessions = session;
                captureRequestBuilder.removeTarget(reader.getSurface());
                captureRequestBuilder.build();
                updatePreview();

              }
            }, null);
          } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, "Something went wrong", Toast.LENGTH_SHORT).show();
            createCameraPreview();
          }
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
        }
      }, mBackgroundHandler);
      showPathFile("Photo save in path -" + file.getPath());
      soundCamera();


    } catch (CameraAccessException e) {
      e.printStackTrace();
    }
  }

  private void showPathFile(String s) {
    ObjectAnimator anim = ObjectAnimator.ofFloat(flashing, "alpha", 0.0f);
    anim.setDuration(200); // duration 3 seconds
    anim.addListener(new Animator.AnimatorListener() {
      @Override
      public void onAnimationStart(Animator animator) {
        flashing.setVisibility(VISIBLE);
      }

      @Override
      public void onAnimationEnd(Animator animator) {
        flashing.setVisibility(INVISIBLE);
        flashing.setAlpha(0.8f);
      }

      @Override
      public void onAnimationCancel(Animator animator) {

      }

      @Override
      public void onAnimationRepeat(Animator animator) {

      }
    });
    anim.start();

    // Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
  }


  CameraCaptureSession.CaptureCallback captureListener;
  Surface surface;

  protected void createCameraPreview() {
    try {

      closePreviewSession();
      final SurfaceTexture texture = getSurfaceTexture();
      texture.setDefaultBufferSize(imageDimension.getWidth(), imageDimension.getHeight());
      if (captureRequestBuilder == null) {
        captureRequestBuilder =
          cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
      } else {
        captureRequestBuilder.removeTarget(surface);
        if (reader != null)
          captureRequestBuilder.removeTarget(reader.getSurface());
        captureRequestBuilder.build();
      }
      surface = new Surface(texture);
      captureRequestBuilder.addTarget(surface);
      initZoom();
      cameraDevice.createCaptureSession(Collections.singletonList(surface), new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
          if (null == cameraDevice) {
            return;
          }
          cameraCaptureSessions = cameraCaptureSession;
          updatePreview();
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
        }
      }, null);
    } catch (CameraAccessException e) {
      e.printStackTrace();
    }
  }

  private Rect zoom;

  private int brightness;

  public void setBrightness(int value) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_DENIED) {
      CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
      try {
        if (cameraId == null) {
          cameraId = mCameraId;
        }
        CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
        int minCompensationRange = getExposureTimeRange(characteristics).getLower();
        int maxCompensationRange = getExposureTimeRange(characteristics).getUpper();
        brightness = (int) (minCompensationRange + (maxCompensationRange - minCompensationRange) * (value / 100f));
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, brightness);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        captureRequestBuilder.set(CaptureRequest.CONTROL_AE_LOCK, true);
        initZoom();
        applySettings();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  private static Range<Integer> getExposureTimeRange(CameraCharacteristics cameraCharacteristics) {
    return cameraCharacteristics.get(CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE);
  }


  private void applySettings() {
    try {
      cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), captureListener, mBackgroundHandler);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  private ClickCamera clickCamera;

  public interface ClickCamera {
    void click(float x, float y);

    void focusAdd();

    void closeFocus();
  }

  public void setClickCamera(ClickCamera clickCamera) {
    this.clickCamera = clickCamera;
  }

  private boolean zoomTouch = false;
  private Handler handler;

  public float fingerSpacing = 0;
  public float zoomLevel = 1f;
  public float maximumZoomLevel;


  @SuppressLint("ClickableViewAccessibility")
  @Override
  public boolean onTouchEvent(MotionEvent event) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_DENIED) {

      try {
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        if (cameraId == null) {
          cameraId = mCameraId;
        }
        CameraCharacteristics characteristics = Objects.requireNonNull(manager).getCameraCharacteristics(cameraId);
        float maximumZoomLevel = characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM) * 6;
        int action = event.getAction();

        if (event.getPointerCount() > 1) {
          zoomTouch = true;
          Rect rect = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
          if (rect == null) return false;
          float currentFingerSpacing;
          currentFingerSpacing = getFingerSpacing(event);
          float delta = 0.05f;
          if (fingerSpacing != 0) {
            if (currentFingerSpacing > fingerSpacing) {
              if ((maximumZoomLevel - zoomLevel) <= delta) {
                delta = maximumZoomLevel - zoomLevel;
              }
              zoomLevel = zoomLevel + delta;
            } else if (currentFingerSpacing < fingerSpacing) {
              if ((zoomLevel - delta) < 1f) {
                delta = zoomLevel - 1f;
              }
              zoomLevel = zoomLevel - delta;
            }
            float ratio = (float) 1 / zoomLevel;
            int croppedWidth = rect.width() - Math.round((float) rect.width() * ratio);
            int croppedHeight = rect.height() - Math.round((float) rect.height() * ratio);
            zoom = new Rect(croppedWidth / 2, croppedHeight / 2,
              rect.width() - croppedWidth / 2, rect.height() - croppedHeight / 2);
            captureRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, zoom);
          }
          fingerSpacing = currentFingerSpacing;
        } else if (event.getPointerCount() == 1) {
          if (action == MotionEvent.ACTION_UP) {
            if (handler != null) {
              clearAll();
              return true;
            }

            if (!zoomTouch) {
              handler = new Handler();
              handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                  handler = null;
                }
              }, 300);
            }
            if (zoomTouch) {
              zoomTouch = false;
              return true;
            }

            clickCamera.click(event.getX(), event.getY());

            CameraManager manager1 = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
            CameraCharacteristics characteristics1 = manager1.getCameraCharacteristics(cameraId);
            final Rect sensorArraySize = characteristics1.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);

            //TODO: here I just flip x,y, but this needs to correspond with the sensor orientation (via SENSOR_ORIENTATION)
            final int y = (int) ((event.getX() / (float) this.getWidth()) * (float) sensorArraySize.height());
            final int x = (int) ((event.getY() / (float) this.getHeight()) * (float) sensorArraySize.width());
            final int halfTouchWidth = (int) event.getTouchMajor(); //(int)motionEvent.getTouchMajor(); //TODO: this doesn't represent actual touch size in pixel. Values range in [3, 10]...
            final int halfTouchHeight = (int) event.getTouchMinor(); //(int)event.getTouchMinor()
            focusAreaTouch = new MeteringRectangle(Math.max(x - halfTouchWidth, 0),
              Math.max(y - halfTouchHeight, 0),
              halfTouchWidth * 2,
              halfTouchHeight * 2,
              MeteringRectangle.METERING_WEIGHT_MAX - 1);


            if (captureRequestBuilder != null)
              initSetting();
            return true;
          }
        }

        try {
          cameraCaptureSessions
            .setRepeatingRequest(captureRequestBuilder.build(), captureListener, mBackgroundHandler);
        } catch (CameraAccessException e) {
          e.printStackTrace();
        } catch (NullPointerException ex) {
          ex.printStackTrace();
        } catch (IllegalStateException exx) {
          exx.printStackTrace();
        }
      } catch (CameraAccessException e) {
        throw new RuntimeException("can not access camera.", e);
      }
      return true;
    }
    return true;
  }

  private MeteringRectangle focusAreaTouch;

  private void clearAll() {
    clickCamera.closeFocus();
    focusAreaTouch = null;
    if (isEnable) {
      captureRequestBuilder = null;
      createCameraPreview();
    } else {
      try {
        if (mIsRecordingVideo) {
          mIsRecordingVideo = false;
          mMediaRecorder.stop();
          mMediaRecorder.reset();
          previewSurface.release();
          captureRequestBuilder.removeTarget(previewSurface);
          captureRequestBuilder.removeTarget(mRecorderSurface);
          previewSurface = null;
          onRecordingStopped(mNextVideoAbsolutePath);
          mNextVideoAbsolutePath = null;
          captureRequestBuilder.build();
          captureRequestBuilder = null;
          /// Surface for MediaRecorder
          startPreview();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  CameraCaptureSession.CaptureCallback captureCallbackHandler = new CameraCaptureSession.CaptureCallback() {
    @Override
    public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                   @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
      super.onCaptureCompleted(session, request, result);
      if (Objects.equals(request.getTag(), "FOCUS_TAG")) {
        clickCamera.focusAdd();
        //the focus trigger is complete -
        //resume repeating (preview surface will get frames), clear AF trigger

        try {
          captureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, null);
          cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), captureListener, mBackgroundHandler);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }

    @Override
    public void onCaptureFailed(@NonNull CameraCaptureSession session,
                                @NonNull CaptureRequest request,
                                @NonNull CaptureFailure failure) {
      super.onCaptureFailed(session, request, failure);
      Log.e(TAG, "Manual AF failure: " + failure);
    }
  };

  private boolean isMeteringAreaAFSupported() {
    CameraManager manager1 = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
    CameraCharacteristics characteristics1 = null;
    try {
      characteristics1 = manager1.getCameraCharacteristics(cameraId);
    } catch (CameraAccessException e) {
      e.printStackTrace();
    }
    return characteristics1.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF) >= 1;
  }


  //Determine the space between the first two fingers
  @SuppressWarnings("deprecation")
  private float getFingerSpacing(MotionEvent event) {
    float x = event.getX(0) - event.getX(1);
    float y = event.getY(0) - event.getY(1);
    return (float) Math.sqrt(x * x + y * y);
  }


  /**
   * Max preview width that is guaranteed by Camera2 API
   */
  private static final int MAX_PREVIEW_WIDTH = 1920;

  /**
   * Max preview height that is guaranteed by Camera2 API
   */
  private static final int MAX_PREVIEW_HEIGHT = 1080;

  /**
   * Sets up member variables related to camera.
   *
   * @param width  The width of available size for camera preview
   * @param height The height of available size for camera preview
   */
  @SuppressWarnings("SuspiciousNameCombination")
  private void setUpCameraOutputs(int width, int height) {
    CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
    try {
      for (String cameraId : manager.getCameraIdList()) {
        CameraCharacteristics characteristics
          = manager.getCameraCharacteristics(cameraId);

        // We don't use a front facing camera in this sample.
        Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
        if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
          continue;
        }

        StreamConfigurationMap map = characteristics.get(
          CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        if (map == null) {
          continue;
        }

        // For still image captures, we use the largest available size.
        Size largest = Collections.max(
          Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
          new RNAndroidCamera2View.CompareSizesByArea());

        // Find out if we need to swap dimension to get the preview size relative to sensor
        // coordinate.
        int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        //noinspection ConstantConditions
        mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        boolean swappedDimensions = false;
        switch (displayRotation) {
          case Surface.ROTATION_0:
          case Surface.ROTATION_180:
            if (mSensorOrientation == 90 || mSensorOrientation == 270) {
              swappedDimensions = true;
            }
            break;
          case Surface.ROTATION_90:
          case Surface.ROTATION_270:
            if (mSensorOrientation == 0 || mSensorOrientation == 180) {
              swappedDimensions = true;
            }
            break;
          default:
            Log.e(TAG, "Display rotation is invalid: " + displayRotation);
        }

        Point displaySize = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
        int rotatedPreviewWidth = width;
        int rotatedPreviewHeight = height;
        int maxPreviewWidth = displaySize.x;
        int maxPreviewHeight = displaySize.y;

        if (swappedDimensions) {
          rotatedPreviewWidth = height;
          rotatedPreviewHeight = width;
          maxPreviewWidth = displaySize.y;
          maxPreviewHeight = displaySize.x;
        }

        if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
          maxPreviewWidth = MAX_PREVIEW_WIDTH;
        }

        if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
          maxPreviewHeight = MAX_PREVIEW_HEIGHT;
        }

        // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
        // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
        // garbage capture data.
        mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
          rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth,
          maxPreviewHeight, largest);

        // We fit the aspect ratio of TextureView to the size of preview we picked.
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
          this.setAspectRatio(
            mPreviewSize.getWidth(), mPreviewSize.getHeight());
        } else {
          this.setAspectRatio(
            mPreviewSize.getHeight(), mPreviewSize.getWidth());
        }

        // Check if the flash is supported.

        mCameraId = cameraId;
        return;
      }
    } catch (CameraAccessException e) {
      e.printStackTrace();
    } catch (NullPointerException e) {
      // Currently an NPE is thrown when the Camera2API is used but not supported on the
      // device this code runs.
    }
  }

  private static Size chooseOptimalSize(Size[] choices, int textureViewWidth,
                                        int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

    // Collect the supported resolutions that are at least as big as the preview Surface
    List<Size> bigEnough = new ArrayList<>();
    // Collect the supported resolutions that are smaller than the preview Surface
    List<Size> notBigEnough = new ArrayList<>();
    int w = aspectRatio.getWidth();
    int h = aspectRatio.getHeight();
    for (Size option : choices) {
      if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
        option.getHeight() == option.getWidth() * h / w) {
        if (option.getWidth() >= textureViewWidth &&
          option.getHeight() >= textureViewHeight) {
          bigEnough.add(option);
        } else {
          notBigEnough.add(option);
        }
      }
    }

    // Pick the smallest of those big enough. If there is no one big enough, pick the
    // largest of those not big enough.
    if (bigEnough.size() > 0) {
      return Collections.min(bigEnough, new RNAndroidCamera2View.CompareSizesByArea());
    } else if (notBigEnough.size() > 0) {
      return Collections.max(notBigEnough, new RNAndroidCamera2View.CompareSizesByArea());
    } else {
      Log.e(TAG, "Couldn't find any suitable preview size");
      return choices[0];
    }
  }


  private void openCamera(int width, int height) {
    if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_DENIED) {

      setUpCameraOutputs(width, height);
      configureTransform(width, height);

      CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
      Log.e(TAG, "is camera open");
      try {
        CameraCharacteristics characteristics = null;
        for (String cameraId : manager.getCameraIdList()) {
          characteristics = manager.getCameraCharacteristics(cameraId);
          if (characteristics.get(CameraCharacteristics.LENS_FACING) == mFacing) {
            PhotoCameraView.this.cameraId = cameraId;
            break;
          }
        }
        StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
        assert map != null;

        imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];
        // Add permission for camera and let user grant the permission
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
          ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
          return;
        }
        manager.openCamera(cameraId, stateCallback, null);
      } catch (CameraAccessException e) {
        e.printStackTrace();
      }
      Log.e(TAG, "openCamera X");
    }
  }

  protected void updatePreview() {
    if (null == cameraDevice) {
      Log.e(TAG, "updatePreview error, return");
      return;
    }
    try {
      CaptureRequest captureRequest = captureRequestBuilder.build();
      cameraCaptureSessions.setRepeatingRequest(captureRequest, null, mBackgroundHandler);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private void updatePreviewVideo() {
    if (cameraDevice == null) {
      return;
    }
    try {
      HandlerThread thread = new HandlerThread("CameraPreview");
      thread.start();
      cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(), null, mBackgroundHandler);
    } catch (Exception e) {
      e.printStackTrace();
      onCameraAccessException();
    }
  }


  private void closeCamera() {
    if (null != cameraDevice) {
      cameraDevice.close();
      cameraDevice = null;
    }
    if (null != imageReader) {
      imageReader.close();
      imageReader = null;
    }
  }

  private void closePreviewSession() {
    if (cameraCaptureSessions != null) {
      cameraCaptureSessions.close();
      cameraCaptureSessions = null;
    }
  }

  Surface mRecorderSurface;
  Surface previewSurface;

  public void initRecord() {
    if (cameraDevice == null || !isAvailable() || mPreviewSize == null) {
      return;
    }
    try {
      // closePreviewSession();
      setUpMediaRecorder();
      SurfaceTexture texture = getSurfaceTexture();
      assert texture != null;
      texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
      if (captureRequestBuilder == null)
        captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
      List<Surface> surfaces = new ArrayList<>();

      // Surface for camera preview
      previewSurface = new Surface(texture);
      surfaces.add(previewSurface);
      captureRequestBuilder.addTarget(previewSurface);

      /// Surface for MediaRecorder
      mRecorderSurface = mMediaRecorder.getSurface();
      surfaces.add(mRecorderSurface);
      captureRequestBuilder.addTarget(mRecorderSurface);
      cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession session) {
          cameraCaptureSessions = session;
          // initSetting();
          updatePreviewVideo();
          Log.d("MWIT", "Recording started: " + mNextVideoAbsolutePath);
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
          Log.e("MWIT", "Recording failed!");
          onCameraFailed();
        }
      }, mBackgroundHandler);

    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
      onCameraAccessException();
    }
  }

  @Override
  void record() {
    mMediaRecorder.start();
    mIsRecordingVideo = true;
    onRecordingStarted();
  }

  private MediaRecorder mMediaRecorder;
  private String mNextVideoAbsolutePath;
  private int mVideoEncodingBitrate;
  private Integer mSensorOrientation;
  private static final int SENSOR_ORIENTATION_DEFAULT_DEGREES = 90;
  private static final int SENSOR_ORIENTATION_INVERSE_DEGREES = 270;
  private static final SparseIntArray DEFAULT_ORIENTATIONS = new SparseIntArray();
  private static final SparseIntArray INVERSE_ORIENTATIONS = new SparseIntArray();

  static {
    DEFAULT_ORIENTATIONS.append(Surface.ROTATION_0, 90);
    DEFAULT_ORIENTATIONS.append(Surface.ROTATION_90, 0);
    DEFAULT_ORIENTATIONS.append(Surface.ROTATION_180, 270);
    DEFAULT_ORIENTATIONS.append(Surface.ROTATION_270, 180);
  }

  static {
    INVERSE_ORIENTATIONS.append(Surface.ROTATION_0, 270);
    INVERSE_ORIENTATIONS.append(Surface.ROTATION_90, 180);
    INVERSE_ORIENTATIONS.append(Surface.ROTATION_180, 90);
    INVERSE_ORIENTATIONS.append(Surface.ROTATION_270, 0);
  }

  private boolean mIsRecordingVideo = false;
  private int mVideoEncodingFrameRate;
  private Semaphore mCameraOpenCloseLock = new Semaphore(1);
  private int mFacing = CameraCharacteristics.LENS_FACING_BACK;
  private String mCameraId;
  private Size mPreviewSize;

  private void openCameraVideo(int width, int height) {
    mVideoEncodingBitrate = 7000000;
    mVideoEncodingFrameRate = 60;
    // TODO: Add M permission support
    if (activity == null || activity.isFinishing()) {
      return;
    }


    CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);

    try {
//            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
//                throw new RuntimeException("Time out waiting to lock camera opening.");
//            }

      CameraCharacteristics characteristics = null;
      for (String cameraId : manager.getCameraIdList()) {
        characteristics = manager.getCameraCharacteristics(cameraId);
        if (characteristics.get(CameraCharacteristics.LENS_FACING) == mFacing) {
          mCameraId = cameraId;
          break;
        }
      }


      StreamConfigurationMap map = characteristics
        .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
      mSensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
      mVideoSize = chooseVideoSize(map.getOutputSizes(MediaRecorder.class));
      mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
        width, height, mVideoSize);
      int orientation = activity.getResources().getConfiguration().orientation;
      if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
        this.setAspectRatio(mPreviewSize.getWidth(), mPreviewSize.getHeight());
      } else {
        this.setAspectRatio(mPreviewSize.getHeight(), mPreviewSize.getWidth());
      }
      configureTransform(width, height);
      mMediaRecorder = new MediaRecorder();
      manager.openCamera(mCameraId, mStateCallback, null);
    } catch (CameraAccessException e) {
      e.printStackTrace();
      onCameraAccessException();
    }

  }

  private CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {
    @Override
    public void onOpened(CameraDevice camera) {
      cameraDevice = camera;
      startPreview();
      mCameraOpenCloseLock.release();
      configureTransform(getWidth(), getHeight());
    }

    @Override
    public void onDisconnected(CameraDevice camera) {
      mCameraOpenCloseLock.release();
      camera.close();
      cameraDevice = null;
    }

    @Override
    public void onError(CameraDevice camera, int error) {
      mCameraOpenCloseLock.release();
      camera.close();
      cameraDevice = null;
      if (activity != null) {
        activity.finish();
      }
    }
  };

  private void configureTransform(int width, int height) {
    if (mPreviewSize == null || activity == null) {
      return;
    }
    int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
    Matrix matrix = new Matrix();
    RectF viewRect = new RectF(0, 0, width, height);
    RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
    float centerX = viewRect.centerX();
    float centerY = viewRect.centerY();
    if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
      bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
      matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
      float scale = Math.max(
        (float) height / mPreviewSize.getHeight(),
        (float) width / mPreviewSize.getWidth());
      matrix.postScale(scale, scale, centerX, centerY);
      matrix.postRotate(90 * (rotation - 2), centerX, centerY);
    } else {
      bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
      matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
      float scale = Math.max(
        (float) height / mPreviewSize.getWidth(),
        (float) width / mPreviewSize.getHeight());
      matrix.postScale(scale, scale, centerX, centerY);
    }
    this.setTransform(matrix);
  }

  private static Size chooseVideoSize(Size[] choices) {
    for (Size size : choices) {
      if (size.getWidth() == size.getHeight() * 4 / 3 && size.getWidth() <= 1080) {
        return size;
      }
    }
    return choices[choices.length - 1];
  }


  private Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRation) {
    List<Size> bigEnough = new ArrayList<Size>();
    int w = aspectRation.getWidth();
    int h = aspectRation.getHeight();
    for (Size option : choices) {
      if (option.getHeight() == option.getWidth() * h / w &&
        option.getWidth() >= width && option.getHeight() >= height) {
        bigEnough.add(option);
      }
    }

    if (bigEnough.size() > 0) {
      return Collections.min(bigEnough, new RNAndroidCamera2View.CompareSizesByArea());
    } else {
      return choices[0];
    }
  }


  private Size mVideoSize;


  private void setUpMediaRecorder() throws IOException {
    Log.d("MWIT", "Record with frame rate: " + mVideoEncodingFrameRate);
    if (activity == null) {
      return;
    }
    //  mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
    mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
    mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);

    if (mNextVideoAbsolutePath != null) {
      deleteTempFile();
    }
    createFolder();
    mNextVideoAbsolutePath = activity.getExternalFilesDir(Environment.DIRECTORY_DCIM).getPath() + "/.media" + "/VID_" + System.currentTimeMillis() + ".mp4";
    mMediaRecorder.setOutputFile(mNextVideoAbsolutePath);
    mMediaRecorder.setVideoEncodingBitRate(mVideoEncodingBitrate);
    mMediaRecorder.setVideoFrameRate(mVideoEncodingFrameRate);
    mMediaRecorder.setVideoSize(mVideoSize.getWidth(), mVideoSize.getHeight());
    mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
    //  mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
    int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
    switch (mSensorOrientation) {
      case SENSOR_ORIENTATION_DEFAULT_DEGREES:
        mMediaRecorder.setOrientationHint(DEFAULT_ORIENTATIONS.get(rotation));
        break;
      case SENSOR_ORIENTATION_INVERSE_DEGREES:
        mMediaRecorder.setOrientationHint(INVERSE_ORIENTATIONS.get(rotation));
        break;
    }
    mMediaRecorder.prepare();
  }

  @Override
  void stop() {
    try {
      if (mIsRecordingVideo) {
        mIsRecordingVideo = false;
        mMediaRecorder.stop();
        mMediaRecorder.reset();
        previewSurface.release();
        captureRequestBuilder.removeTarget(previewSurface);
        captureRequestBuilder.removeTarget(mRecorderSurface);
        previewSurface = null;
        onRecordingStopped(mNextVideoAbsolutePath);
        showPathFile("Video save in path -" + mNextVideoAbsolutePath);
        mNextVideoAbsolutePath = null;
        captureRequestBuilder.build();
        /// Surface for MediaRecorder
        startPreview();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }


  private void startPreview() {
    if (cameraDevice == null || !isAvailable() || mPreviewSize == null) {
      return;
    }
    try {

      deleteTempFile();
      closePreviewSession();
      SurfaceTexture texture = getSurfaceTexture();
      texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
      if (captureRequestBuilder == null) {
        captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
      } else {
        captureRequestBuilder.removeTarget(previewSurface);
        if (previewSurface != null)
          previewSurface.release();
        if (mRecorderSurface != null) {
          captureRequestBuilder.removeTarget(mRecorderSurface);
          mRecorderSurface.release();
        }
        captureRequestBuilder.build();
      }
      initZoom();
      previewSurface = new Surface(texture);
      captureRequestBuilder.addTarget(previewSurface);

      cameraDevice.createCaptureSession(Collections.singletonList(previewSurface), new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
          PhotoCameraView.this.cameraCaptureSessions = cameraCaptureSession;
          captureRequestBuilder.removeTarget(previewSurface);
          previewSurface.release();
          captureRequestBuilder.build();
          updatePreviewVideo();
          initRecord();
        }

        @Override
        public void onConfigureFailed(@NonNull CameraCaptureSession session) {
          mBase.onCameraFailed();
        }
      }, mBackgroundHandler);
    } catch (CameraAccessException e) {
      e.printStackTrace();
      onCameraAccessException();
    }
  }

  private RNAndroidCamera2Base mBase = this;

  private boolean isEnable;
  private boolean isEnableVideo;

  @Override
  protected void onResume() {
    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA)
      != PackageManager.PERMISSION_GRANTED

      && ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
      != PackageManager.PERMISSION_GRANTED) {
      ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
      return;
    }

    Log.e(TAG, "onResume");
    if (isEnable) {
      startBackgroundThread();
      if (isAvailable()) {
        openCamera(getWidth(), getHeight());
      } else {
        setSurfaceTextureListener(textureListener);
      }
    } else if (isEnableVideo) {
      startBackgroundThread();
      Log.d("MWIT", "onResume");
      if (isAvailable()) {
        openCameraVideo(getWidth(), getHeight());
      } else {
        setSurfaceTextureListener(mSurfaceTextureListener);
      }
    }
  }

  @Override
  protected void onPause() {
    Log.e(TAG, "onPause");
    closeCamera();
    deleteTempFile();
    stopBackgroundThread();
  }

  private void deleteTempFile() {
    if (mNextVideoAbsolutePath != null)
      try {
        new File(mNextVideoAbsolutePath).delete();
      } catch (Exception e) {
        e.printStackTrace();
      }
  }

  @Override
  protected void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    deleteTempFile();
  }

  private boolean photo = true;

  @Override
  void capture() {
    if (photo) {
      photo = false;
      takePicture();
      new Handler().postDelayed(new Runnable() {
        @Override
        public void run() {
          photo = true;
        }
      }, 1000);
    }
  }

  @Override
  void setType(String type) {

  }

  @Override
  void setVideoEncodingBitrate(int bitrate) {
    if (bitrate == 0) {
      mVideoEncodingBitrate = 7000000;
    } else
      mVideoEncodingBitrate = bitrate;
  }

  @Override
  void setVideoEncodingFrameRate(int frameRate) {
    if (frameRate == 0) {
      mVideoEncodingFrameRate = 60;
    } else
      mVideoEncodingFrameRate = frameRate;
  }

  @Override
  boolean isRecording() {
    return false;
  }
}
