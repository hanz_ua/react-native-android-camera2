package com.kombine;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

public class ContainerCamera extends RelativeLayout {
  private RNAndroidCamera2Base rnAndroidCamera2Base;
  private View focus;
  private VerticalSeekBar verticalSeekBar;

  public ContainerCamera(Context context, RNAndroidCamera2Base rnAndroidCamera2Base) {
    super(context);
    this.rnAndroidCamera2Base = rnAndroidCamera2Base;
    addView(rnAndroidCamera2Base);
    View view = LayoutInflater.from(context).inflate(R.layout.seek_bar, null);
    addView(view);
    rnAndroidCamera2Base.setFlashing(view.findViewById(R.id.flashing_view));
    verticalSeekBar = view.findViewById(R.id.mySeekBar);
    verticalSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
      @Override
      public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        // mView.getRnAndroidCamera2Base().setZoomInt(progress);
        getRnAndroidCamera2Base().setBrightness(progress);
      }

      @Override
      public void onStartTrackingTouch(SeekBar seekBar) {

      }

      @Override
      public void onStopTrackingTouch(SeekBar seekBar) {

      }
    });

    rnAndroidCamera2Base.setClickCamera(new PhotoCameraView.ClickCamera() {
      @Override
      public void click(float x, float y) {
        focus.setVisibility(VISIBLE);
        focus.setX(x - 75);
        focus.setY(y - 75);

        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(focus, "scaleX", 1f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(focus, "scaleY", 1f);
        scaleDownX.setDuration(50);
        scaleDownY.setDuration(50);

        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleDown.start();
      }

      @Override
      public void focusAdd() {
        ObjectAnimator scaleDownX = ObjectAnimator.ofFloat(focus, "scaleX", 0.9f);
        ObjectAnimator scaleDownY = ObjectAnimator.ofFloat(focus, "scaleY", 0.9f);
        scaleDownX.setDuration(300);
        scaleDownY.setDuration(300);

        AnimatorSet scaleDown = new AnimatorSet();
        scaleDown.play(scaleDownX).with(scaleDownY);
        scaleDown.start();
      }

      @Override
      public void closeFocus() {
        focus.setVisibility(INVISIBLE);
        verticalSeekBar.setProgress(50);
      }
    });
    focus = new View(getContext());
    focus.setVisibility(INVISIBLE);
    focus.setBackgroundResource(R.mipmap.focus);
    focus.setLayoutParams(new LinearLayout.LayoutParams(
      150,
      150));

    addView(focus);
  }


  public RNAndroidCamera2Base getRnAndroidCamera2Base() {
    return rnAndroidCamera2Base;
  }

  public ContainerCamera(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public ContainerCamera(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
  }

  @TargetApi(Build.VERSION_CODES.LOLLIPOP)
  public ContainerCamera(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
    super(context, attrs, defStyleAttr, defStyleRes);
  }

  void record() {
    rnAndroidCamera2Base.record();
  }

  void stop() {
    rnAndroidCamera2Base.stop();
  }

  void onResume() {
    rnAndroidCamera2Base.onResume();
  }

  void onPause() {
    rnAndroidCamera2Base.onPause();
  }

  void capture() {
    rnAndroidCamera2Base.capture();
  }

  void initPhoto() {
    rnAndroidCamera2Base.initPhoto();
  }

  void initVideo() {
    rnAndroidCamera2Base.initVideo();
  }


}
