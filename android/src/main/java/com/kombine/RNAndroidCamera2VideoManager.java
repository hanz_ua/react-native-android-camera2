package com.kombine;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;

import com.facebook.infer.annotation.Assertions;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.common.MapBuilder;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.h6ah4i.android.widget.verticalseekbar.VerticalSeekBar;

import java.util.Map;

import javax.annotation.Nullable;

public class RNAndroidCamera2VideoManager extends SimpleViewManager<ContainerCamera> implements LifecycleEventListener {
    private static final int COMMAND_RECORD = 559;
    private static final int COMMAND_STOP = 365;
    private static final int COMMAND_CAPTURE = 656;
    private static final int COMMAND_INIT = 768;
    private Activity mActivity;
    private ContainerCamera mView;

    public RNAndroidCamera2VideoManager(Activity activity) {
        mActivity = activity;
    }

    @Override
    public void onHostResume() {
        mView.onResume();
    }

    @Override
    public void onHostPause() {
        mView.onPause();
    }

    @Override
    public void onHostDestroy() {

    }

    @ReactProp(name = "type")
    public void setType(RNAndroidCamera2Base view, @Nullable String type) {
        view.setType(type);
    }

    @ReactProp(name = "videoEncodingBitrate", defaultInt = 7000000)
    public void setVideoEncodingBitrate(RNAndroidCamera2Base view, int bitrate) {
        view.setVideoEncodingBitrate(bitrate);
    }

    @ReactProp(name = "videoEncodingFrameRate", defaultInt = 60)
    public void setVideoEncodingFrameRate(RNAndroidCamera2Base view, int frameRate) {
        view.setVideoEncodingFrameRate(frameRate);
    }

    @Override
    public String getName() {
        return "RNAndroidCamera2Video";
    }

    @Override
    protected ContainerCamera createViewInstance(ThemedReactContext reactContext) {
        mActivity = reactContext.getCurrentActivity();
        reactContext.addLifecycleEventListener(this);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            mView = new ContainerCamera(mActivity, new RNAndroidCamera2View(reactContext, mActivity));
//            View view = LayoutInflater.from(mActivity).inflate(R.layout.seek_bar, null);
//            mView.addView(view);
//            ((VerticalSeekBar) view.findViewById(R.id.mySeekBar)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//                @Override
//                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                    // mView.getRnAndroidCamera2Base().setZoomInt(progress);
//                    mView.getRnAndroidCamera2Base().setBrightness(progress);
//                }
//
//                @Override
//                public void onStartTrackingTouch(SeekBar seekBar) {
//
//                }
//
//                @Override
//                public void onStopTrackingTouch(SeekBar seekBar) {
//
//                }
//            });

        } else {
            Log.i("Version out of date", "FIX ME! Version is < 5.0");
        }


        return mView;
    }


    public static float convertDpToPixel(float dp, Context context) {
        return dp * ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    @Override
    public @Nullable
    Map getExportedCustomDirectEventTypeConstants() {
        return MapBuilder.of(
                "recordingStart", MapBuilder.of("registrationName", "onRecordingStarted"),
                "recordingFinish", MapBuilder.of("registrationName", "onRecordingFinished"),
                "cameraAccessException", MapBuilder.of("registrationName", "onCameraAccessException"),
                "cameraFailed", MapBuilder.of("registrationName", "cameraFailed")
        );
    }

    @Override
    public Map<String, Integer> getCommandsMap() {
        return MapBuilder.of(
                "record", COMMAND_RECORD,
                "stop", COMMAND_STOP,
                "capture", COMMAND_CAPTURE,
                "init", COMMAND_INIT
        );
    }


    @Override
    public void receiveCommand(
            ContainerCamera view,
            int commandType,
            @Nullable ReadableArray args
    ) {
        Assertions.assertNotNull(view);

        switch (commandType) {
            case COMMAND_RECORD:
                view.record();
                break;
            case COMMAND_STOP:
                view.stop();
                break;
            case COMMAND_CAPTURE:
                view.capture();
            case COMMAND_INIT:
        }
    }
}
