package com.kombine;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.View;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;

import com.kombine.AutoFitTextureView;

abstract class RNAndroidCamera2Base extends AutoFitTextureView {

  public RNAndroidCamera2Base(Context context) {
    super(context);
  }

  abstract void record();

  abstract void stop();

  abstract void onResume();

  abstract void onPause();

  abstract void capture();

  abstract void setType(String type);

  abstract void setVideoEncodingBitrate(int bitrate);

  abstract void setVideoEncodingFrameRate(int frameRate);

  public abstract void setFlashing(View flashing);

  abstract boolean isRecording();

  protected void soundCamera() {
    MediaPlayer mPlayer = MediaPlayer.create(getContext(), R.raw.camera);
    mPlayer.start();
  }

  public void onRecordingStarted() {
    WritableMap event = Arguments.createMap();
    event.putBoolean("ok", true);
    ReactContext reactContext = (ReactContext) getContext();
    reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
      getId(),
      "recordingStart",
      event
    );
  }

  public void onRecordingStopped(String path) {
    WritableMap event = Arguments.createMap();
    event.putString("file", path);
    ReactContext reactContext = (ReactContext) getContext();
    reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
      getId(),
      "recordingFinish",
      event
    );
  }

  public void onCameraAccessException() {
    WritableMap event = Arguments.createMap();
    ReactContext reactContext = (ReactContext) getContext();
    reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
      getId(),
      "cameraAccessException",
      event
    );
  }

  public void onCameraFailed() {
    WritableMap event = Arguments.createMap();
    ReactContext reactContext = (ReactContext) getContext();
    reactContext.getJSModule(RCTEventEmitter.class).receiveEvent(
      getId(),
      "cameraFailed",
      event
    );
  }

  public abstract void initPhoto();

  public abstract void initVideo();

  public abstract void setClickCamera(PhotoCameraView.ClickCamera clickCamera);

  public abstract void setBrightness(int progress);
}
