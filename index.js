// @flow
import React, { Component, PropTypes } from 'react';
import ReactNative, {
  requireNativeComponent,
  UIManager,
  StyleSheet,
} from 'react-native';

const NVideoRecorder = requireNativeComponent('RNAndroidCamera2', RNAndroidCamera2Recorder, {});

export default class RNAndroidCamera2Recorder extends Component {
  stop() {
    UIManager.dispatchViewManagerCommand(
      this.getNodeHandle(),
      UIManager.RNAndroidCamera2.Commands.stop,
      null
    );
  }

  record() {
    UIManager.dispatchViewManagerCommand(
      this.getNodeHandle(),
      UIManager.RNAndroidCamera2.Commands.record,
      null
    );
  }

    initPhoto() {
        UIManager.dispatchViewManagerCommand(
            this.getNodeHandle(),
            UIManager.RNAndroidCamera2.Commands.initPhoto,
            null
        );
    }

    initVideo() {
        UIManager.dispatchViewManagerCommand(
            this.getNodeHandle(),
            UIManager.RNAndroidCamera2.Commands.initVideo,
            null
        );
    }

    capture() {
        UIManager.dispatchViewManagerCommand(
            this.getNodeHandle(),
            UIManager.RNAndroidCamera2.Commands.capture,
            null
        );
    }

  getNodeHandle() {
    return ReactNative.findNodeHandle(this.refs.recorder);
  }

  render() {
    return (
      <NVideoRecorder
        {...this.props}
        ref="recorder"
        style={[styles.camera, this.props.style]}
      />
    );
  }
}

// RNAndroidCamera2Recorder.propTypes = {
//   onRecordingStarted: PropTypes.func,
//   onRecordingFinished: PropTypes.func,
//   onCameraAccessException: PropTypes.func,
//   onCameraFailed: PropTypes.func,
//   type: PropTypes.oneOf(['front', 'back']),
//   videoEncodingBitrate: PropTypes.number,
//   videoEncodingFrameRate: PropTypes.number
// };

const styles = StyleSheet.create({
    camera: {
      flex: 1,
    }
  });